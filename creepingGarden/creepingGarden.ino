/*
  This code reads the Analog Voltage output from the
  LV-MaxSonar sensors
*/
#include <SoftwareSerial.h>
#include "RoboClaw.h"

const bool scary = true ; // scary true means it will retract.  Make it false to be friendly
const int furthest = 30; // in inches?
const int closest = 3; //in inches
const int furthestOut= 40; // the maximum number of turns a garden can go out
const int robotSpeed=64; // from 1 to 128
const int avgrange = 5; // sample size
const int anPin1 = 0;
const int anPin2 = 1;
const int anPin3 = 2;
const int pauseLength = 60000; // 60 seconds of pause time edit to change

SoftwareSerial serial(10, 11); // these are the digital pins that will send serial out to the roboclaw
RoboClaw roboclaw(&serial, 10000);

#define address 0x80

int triggerPin1 = 13; // trigger on
int turns;
long d1, d2, d3; // distances
long d1S, d2S, d3S;
bool bearing = true ; // True is doing what it is supposed to do, false is going back

void setup() {
  roboclaw.begin(38400);
  Serial.begin(9600);  // sets the serial port to 9600
  pinMode(triggerPin1, OUTPUT);
}

void start_sensor() {
  digitalWrite(triggerPin1, HIGH);
  delay(1);
  digitalWrite(triggerPin1, LOW);
}

void read_sensors() {
  /*
    Scale factor is (Vcc/512) per inch. A 5V supply yields ~9.8mV/in
    Arduino analog pin goes from 0 to 1024, so the value has to be divided by 2 to get the actual inches
  */

  for (int i = 0; i < avgrange ; i++)
  {
  //  Serial.println(" for loop "+i);
    d1S += analogRead(anPin1) * 5 / 25.4;
    d2S += analogRead(anPin2) * 5 / 25.4;
    d3S += analogRead(anPin3) * 5 / 25.4;
    delay(150); //This is the equivant of the amount of sensors times 50.  If you changed this to 5 sensors the delay would be 250.
  }
  d1 = d1S / avgrange;
  d2 = d2S / avgrange;
  d3 = d3S / avgrange;
  d1S = 0;
  d2S = 0;
  d3S = 0;
}

void print_all() {
  Serial.print(d1);
  Serial.print(",");
  Serial.print(d2);
  Serial.print(",");
  Serial.print(d3);
    Serial.print(",");
      Serial.println(turns);

}

void loop() {
  start_sensor();
  read_sensors();
  roboclaw.ForwardM1 (address, 0);
  roboclaw.ForwardM2 (address, 0);
  if ((d1 < furthest ) or (d2 < furthest ) or (d3 < furthest)) {
    if (turns < furthestOut) {
      if ( (d1 > closest) and  (d2 > closest) and  (d3 > closest)) {
        if  (scary) {
        roboclaw.ForwardM1 (address, robotSpeed);
        roboclaw.ForwardM2 (address, robotSpeed);
        }
        else {
        roboclaw.BackwardM1 (address, robotSpeed);
        roboclaw.BackwardM2 (address, robotSpeed);
        }
        turns = turns + 1;
        bearing = true;
      }
    }
  } else {
      Serial.println("going back");
      if (bearing) {
        delay (pauseLength);
        bearing = false;
      }
      if (turns > 0) { // go back
  
        if  (scary) {
        roboclaw.BackwardM1 (address, robotSpeed);
        roboclaw.BackwardM2 (address, robotSpeed);
        }
        else {
        roboclaw.ForwardM1 (address, robotSpeed);
        roboclaw.ForwardM2 (address, robotSpeed);
        }
        turns = turns - 1;
    }

  }
  print_all();
}
