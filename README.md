# Halsey

# **Creeping Garden**
## What it does

A self-controlled chasis moves backwards and forwards depending on detection of people in front of it.

## How it does it

An arduino receives distance readings from 3 sensors located in front.  
The arduino controls 4 motors, one on each wheel
Everything is mounted on an aluminum chasis on which a platform is mounted where the garden is attached.

### Specifics
The sensors are three sonar rangers placed in an arc to create a wide detection area
The arduino activates and polls each sensor one at a time.
If it is marked as Scary= True then if it detects an object closer than the maximum range and further than the minimum it will retract.  If not it will expand to its original distance
If Scary = False it will do the opposite, extending when it detects someone and retracting when it doesn't.
The arduino controls the motors by sending commands to a little red board (the RoboClaw) that controls the motors.

### Installation
We are using the RoboClaw library (as well as software serial)

Download the code 
Place both folders like they are in a folder.

Make sure the Libraries folder is alongside creepingGarden

for example my installation is as follows:


      .
      └──halsey_code     (a folder on my computer)
         ├──creepingGarden  ( creepingGarden.ino lives here)
         └──Libraries  ( RoboClaw.cpp and RoboClaw.h  live here )


Connect a USB cable to the Arduino and upload the code.  The Arduinos currently have the code on them, but you will need to configure them so you might as well do this step.

### Configuring
The first part of the code is where you can configure the variables.
By the way I did this in Arduino 1.8.9.  But there is nothing strange about this, it will probably work with earlier versions if you don't want to update.

To configure this properly I highly recommend that you explore the numbers that come from the sensors first keeping the arduino plugged in to the computer and using the serial monitor.   

The serial monitor shows 3 numbers, the first sensor reading, the second, the third and the "turns" or "distance" it has travelled. 

Aim the sensors at something some 15 feet away.  That is your furthest,  Now put something a foot or two in front of the sensors.  That is your closest.

*Don't be like me and go crazy trying to figure out the real relationship / formula between inches and the numbers.  It varies.  In ideal circumstances it works to 1 number = 1 inch, but ideal circumstances are with a perfect 5 volt input and no wire length between the sensor and the arduino.  None of those exist in reality.*

I like line numbers,  you can enable them in the preferences 

```arduino 
Line 8:
const bool scary = True ; 
``` 
The garden has two modes scary (where it "flees" people) and friendly (scaru = False) where it is attracted by people.  Some of the gardens will be scary others wont.  Change accordingly

```arduino  
Lines 9 and 10:
const int furthest = 8; //in inches
const int closest = 3; //in inches
```
These are the outer detection limit (furthest) and the inner (too close to react, just freeze, aka closest)  

```arduino  
Line 11:
const int furthestOut= 40;
```
This is the maximum number of turns the robot can do before running out of space.  I *think*  40 is correct but don't believe me.  

```arduino  
Line 12:
const int robotSpeed=64;  
```
This is the speed of the robot.  64 is right in the middle.  128 is Maximum speed.  Adjust accordingly in the room, but you want it to be slow, a plant that size needs to move slow to not be intimidating.

```arduino  
Line 13:
const int avgrange = 5;
```
This is a "smoothing" function.   The code takes (in this case) 5 readings and averages out the numbers (that is why it is a bit slow to change, which is not a bad thing for a living creature)  You can adjust it if needed, but don't make it too big. 


## Gotchas
* Sensors might interact with each other from different gardens if they are too close.
* Sensors are "self adjusting"  They will take a couple of readings first in case there are static objects so that it can ignore them.  Most of the time it works but not always.  BUT it can also cause a slight problem:  If you see that a sensor is reading consistently 2, or 0 or another small number but not a larger one no matter how far you aim it, "force" it to readjust by covering the sensor with your thumb for 10 seconds or so.   Once you remove it , it should start giving real readings.  You should only have to do this once if at all.* 


I've kept udating the 3d model.  
Link to 3d Model:
https://a360.co/2JaeQUC 